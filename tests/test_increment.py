import json
import unittest
import os.path
import sys

import numpy

import primitive_interfaces
from d3m_metadata import container, metadata, utils

TEST_PRIMITIVES_DIR = os.path.join(os.path.dirname(__file__), 'data', 'primitives')

sys.path.insert(0, TEST_PRIMITIVES_DIR)

from test_primitives.increment import IncrementPrimitive


EXPECTED_PRIMITIVE_DESCRIPTION_JSON = r"""
{
    "id": "5c9d5acf-7754-420f-a49f-90f4d9d0d694",
    "version": "0.1.0",
    "digest": "8b63e249b79893622d2655d78df00864a8bc901dfb6b6f0c34b9580e5bf5fa7e",
    "name": "Increment Values",
    "keywords": [
        "test primitive"
    ],
    "source": {
        "name": "Test team",
        "uris": [
            "https://gitlab.com/datadrivendiscovery/tests-data/blob/master/primitives/test_primitives/increment.py",
            "https://gitlab.com/datadrivendiscovery/tests-data.git"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://gitlab.com/datadrivendiscovery/tests-data.git@__GIT_COMMIT__#egg=test_primitives&subdirectory=primitives"
        }
    ],
    "location_uris": [
        "https://gitlab.com/datadrivendiscovery/tests-data/raw/__GIT_COMMIT__/primitives/test_primitives/increment.py"
    ],
    "python_path": "d3m.primitives.test.IncrementPrimitive",
    "algorithm_types": [
        "COMPUTER_ALGEBRA"
    ],
    "primitive_family": "OPERATOR",
    "preconditions": [
        "NO_MISSING_VALUES",
        "NO_CATEGORICAL_VALUES"
    ],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "test_primitives.increment.IncrementPrimitive",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m_metadata.container.numpy.ndarray",
            "Outputs": "d3m_metadata.container.numpy.ndarray",
            "Hyperparams": "test_primitives.increment.Hyperparams",
            "Params": "NoneType"
        },
        "interfaces_version": "__INTERFACES_VERSION__",
        "interfaces": [
            "transformer.TransformerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "amount": {
                "type": "d3m_metadata.hyperparams.Hyperparameter",
                "default": 1,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ]
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "test_primitives.increment.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "docker_containers": {
                "type": "typing.Union[typing.Dict[str, str], NoneType]",
                "kind": "RUNTIME",
                "default": "None"
            },
            "timeout": {
                "type": "typing.Union[float, NoneType]",
                "kind": "RUNTIME",
                "default": "None"
            },
            "iterations": {
                "type": "typing.Union[int, NoneType]",
                "kind": "RUNTIME",
                "default": "None"
            },
            "inputs": {
                "type": "d3m_metadata.container.numpy.ndarray",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "NoneType",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {
            "can_accept": {
                "arguments": {
                    "method_name": {
                        "type": "str"
                    },
                    "arguments": {
                        "type": "typing.Dict[str, typing.Union[d3m_metadata.metadata.Metadata, type]]"
                    }
                },
                "returns": "typing.Union[d3m_metadata.metadata.DataMetadata, NoneType]",
                "description": "Returns a metadata object describing the output of a call of ``method_name`` method with\narguments ``arguments``, if such arguments can be accepted by the method. Otherwise it\nreturns ``None``.\n\nDefault implementation checks structural types of ``arguments`` to match method's arguments' types.\n\nBy (re)implementing this method, a primitive can fine-tune which arguments it accepts\nfor its methods which goes beyond just structural type checking. For example, a primitive might\noperate only on images, so it can accept NumPy arrays, but only those with semantic type\ncorresponding to an image. Or it might check dimensions of an array to assure it operates\non square matrix.\n\nParameters\n----------\nmethod_name : str\n    Name of the method which would be called.\narguments : Dict[str, Union[Metadata, type]]\n    A mapping between argument names and their metadata objects (for pipeline arguments) or types (for other).\n\nReturns\n-------\nDataMetadata\n    Metadata object of the method call result, or ``None`` if arguments are not accepted\n    by the method."
            }
        },
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed",
                    "docker_containers"
                ],
                "returns": "NoneType",
                "description": "All primitives should accept all their hyper-parameters in a constructor as one value,\nan instance of type ``Hyperparams``.\n\nMethods can accept per-call overrides for some hyper-parameters (e.g., a threshold\nfor fitting). Those arguments have to match in name and type a hyper-parameter defined\nin ``hyperparams`` object. The value provided in the ``hyperparams`` object serves\nas a default in such case.\n\nProvided random seed should control all randomness used by this primitive.\nPrimitive should behave exactly the same for the same random seed across multiple\ninvocations. You can call `numpy.random.RandomState(random_seed)` to obtain an\ninstance of a random generator using provided seed.\n\nPrimitives can be wrappers around or use one or more Docker images which they can\nspecify as part of  ``installation`` field in their metadata. Each Docker image listed\nthere has a ``key`` field identifying that image. When primitive is created,\n``docker_containers`` contains a mapping between those keys and addresses to which\nprimitive can connect to access a running Docker container for a particular Docker\nimage. Docker containers might be long running and shared between multiple instances\nof a primitive.\n\nNo other arguments to the constructor are allowed (except for private arguments)\nbecause we want instances of primitives to be created without a need for any other\nprior computation.\n\nConstructor should be kept lightweight and not do any computation. No resources\nshould be allocated in the constructor. Resources should be allocated when needed."
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "NoneType",
                "description": "A noop.\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "NoneType",
                "description": "A noop.\n\nReturns\n-------\nParams\n    An instance of parameters."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "primitive_interfaces.base.CallResult[d3m_metadata.container.numpy.ndarray]",
                "description": "Produce primitive's best choice of the output for each of the inputs.\n\nThe output value should be wrapped inside ``CallResult`` object before returning.\n\nIn many cases producing an output is a quick operation in comparison with ``fit``, but not\nall cases are like that. For example, a primitive can start a potentially long optimization\nprocess to compute outputs. ``timeout`` and ``iterations`` can serve as a way for a caller\nto guide the length of this process.\n\nIdeally, a primitive should adapt its call to try to produce the best outputs possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore producing outputs, it should raise a ``TimeoutError`` exception to signal that the\ncall was unsuccessful in the given time. The state of the primitive after the exception\nshould be as the method call has never happened and primitive should continue to operate\nnormally. The purpose of ``timeout`` is to give opportunity to a primitive to cleanly\nmanage its state instead of interrupting execution from outside. Maintaining stable internal\nstate should have precedence over respecting the ``timeout`` (caller can terminate the\nmisbehaving primitive from outside anyway). If a longer ``timeout`` would produce\ndifferent outputs, then ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal iterations (for example, optimization iterations).\nFor those, caller can provide how many of primitive's internal iterations\nshould a primitive do before returning outputs. Primitives should make iterations as\nsmall as reasonable. If ``iterations`` is ``None``, then there is no limit on\nhow many iterations the primitive should do and primitive should choose the best amount\nof iterations on its own (potentially controlled through hyper-parameters).\nIf ``iterations`` is a number, a primitive has to do those number of iterations,\nif possible. ``timeout`` should still be respected and potentially less iterations\ncan be done because of that. Primitives with internal iterations should make\n``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should run fully, respecting only ``timeout``.\n\nParameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    The outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "A noop.\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "NoneType",
                "description": "A noop.\n\nParameters\n----------"
            }
        },
        "class_attributes": {
            "metadata": "d3m_metadata.metadata.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m_metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, str]"
        }
    },
    "structural_type": "test_primitives.increment.IncrementPrimitive",
    "description": "A primitive which increments each value by a fixed amount, by default 1.\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, str]\n    A dict mapping Docker image keys from primitive's metadata to container addresses\n    under which containers are accessible by the primitive."
}
""".replace('__INTERFACES_VERSION__', primitive_interfaces.__version__).replace('__GIT_COMMIT__', utils.current_git_commit(TEST_PRIMITIVES_DIR))


class TestIncrementPrimitive(unittest.TestCase):
    # It is not necessary to call "can_accept" before calling a method
    # during runtime, but we are doing it here for testing purposes.
    def call_primitive(self, primitive, method_name, **kwargs):
        primitive_arguments = primitive.metadata.query()['primitive_code']['arguments']

        arguments = {}

        for argument_name, argument_value in kwargs.items():
            if primitive_arguments[argument_name]['kind'] == metadata.PrimitiveArgumentKind.PIPELINE:
                arguments[argument_name] = argument_value.metadata
            else:
                arguments[argument_name] = type(argument_value)

        self.assertTrue(type(primitive).can_accept(
            method_name=method_name,
            arguments=arguments,
        ))

        return getattr(primitive, method_name)(**kwargs)

    def test_basic(self):
        hyperparams_class = IncrementPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        primitive = IncrementPrimitive(hyperparams=hyperparams_class.defaults())

        inputs = container.ndarray([[1, 2, 3, 4], [5, 6, 7, 8]], {
            'schema': metadata.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.ndarray,
            'dimension': {
                'length': 2,
            },
        })
        inputs.metadata = inputs.metadata.update((metadata.ALL_ELEMENTS,), {
            'dimension': {
                'length': 4,
            },
        })
        inputs.metadata = inputs.metadata.update((metadata.ALL_ELEMENTS, metadata.ALL_ELEMENTS), {
            'structural_type': inputs.dtype.type,
        })

        call_metadata = self.call_primitive(primitive, 'produce', inputs=inputs)

        self.assertTrue(numpy.array_equal(call_metadata.value, container.ndarray([[2, 3, 4, 5], [6, 7, 8, 9]])))
        self.assertEqual(call_metadata.has_finished, True)
        self.assertEqual(call_metadata.iterations_done, None)

        self.assertIs(call_metadata.value.metadata.for_value, call_metadata.value)
        self.assertEqual(call_metadata.value.metadata.query(())['dimension']['length'], 2)
        self.assertEqual(call_metadata.value.metadata.query((metadata.ALL_ELEMENTS,))['dimension']['length'], 4)
        self.assertEqual(call_metadata.value.metadata.query((metadata.ALL_ELEMENTS, metadata.ALL_ELEMENTS))['structural_type'], numpy.int64)

    def test_hyperparameter(self):
        hyperparams_class = IncrementPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        primitive = IncrementPrimitive(hyperparams=hyperparams_class(amount=2))

        inputs = container.ndarray([[1, 2, 3, 4], [5, 6, 7, 8]], {
            'schema': metadata.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.ndarray,
            'dimension': {
                'length': 2,
            },
        })
        inputs.metadata = inputs.metadata.update((metadata.ALL_ELEMENTS,), {
            'dimension': {
                'length': 4,
            },
        })
        inputs.metadata = inputs.metadata.update((metadata.ALL_ELEMENTS, metadata.ALL_ELEMENTS), {
            'structural_type': inputs.dtype.type,
        })

        call_metadata = self.call_primitive(primitive, 'produce', inputs=inputs)

        self.assertTrue(numpy.array_equal(call_metadata.value, container.ndarray([[3, 4, 5, 6], [7, 8, 9, 10]])))
        self.assertEqual(call_metadata.has_finished, True)
        self.assertEqual(call_metadata.iterations_done, None)

        self.assertIs(call_metadata.value.metadata.for_value, call_metadata.value)
        self.assertEqual(call_metadata.value.metadata.query(())['dimension']['length'], 2)
        self.assertEqual(call_metadata.value.metadata.query((metadata.ALL_ELEMENTS,))['dimension']['length'], 4)
        self.assertEqual(call_metadata.value.metadata.query((metadata.ALL_ELEMENTS, metadata.ALL_ELEMENTS))['structural_type'], numpy.int64)

    def test_can_accept(self):
        inputs_metadata = metadata.DataMetadata({
            'schema': metadata.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.ndarray,
        })

        self.assertFalse(IncrementPrimitive.can_accept(method_name='produce', arguments={
            'inputs': inputs_metadata,
        }))

        inputs_metadata.update((), {
            'dimension': {
                'length': 2,
            },
        })

        self.assertFalse(IncrementPrimitive.can_accept(method_name='produce', arguments={
            'inputs': inputs_metadata,
        }))

        inputs_metadata.update((metadata.ALL_ELEMENTS,), {
            'dimension': {
                'length': 2,
            },
        })

        self.assertFalse(IncrementPrimitive.can_accept(method_name='produce', arguments={
            'inputs': inputs_metadata,
        }))

        inputs_metadata.update((metadata.ALL_ELEMENTS, metadata.ALL_ELEMENTS), {
            'structural_type': str,
        })

        self.assertFalse(IncrementPrimitive.can_accept(method_name='produce', arguments={
            'inputs': inputs_metadata,
        }))

        inputs_metadata.update((metadata.ALL_ELEMENTS, metadata.ALL_ELEMENTS), {
            'structural_type': float,
        })

        self.assertFalse(IncrementPrimitive.can_accept(method_name='produce', arguments={
            'inputs': inputs_metadata,
        }))

    def test_metadata(self):
        expected_description = json.loads(EXPECTED_PRIMITIVE_DESCRIPTION_JSON)

        # We stringify to JSON and parse it to make sure the description can be stringified to JSON.
        description = json.loads(json.dumps(IncrementPrimitive.metadata.to_json()))

        self.maxDiff = None
        self.assertEqual(expected_description, description)


if __name__ == '__main__':
    unittest.main()
